package com.kaiohenrique.empresasandroidioasys.data.investor.database

import com.kaiohenrique.empresasandroidioasys.data.investor.database.entity.InvestorEntity
import com.kaiohenrique.empresasandroidioasys.data.mapper.toDomain
import com.kaiohenrique.empresasandroidioasys.data.mapper.toEntity
import com.kaiohenrique.empresasandroidioasys.data.realm.RealmBridge
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome
import java.io.IOException


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class InvestorRealmDatabase(
    private val realmBridge: RealmBridge
) : InvestorDatabase {

    override suspend fun save(investor: Investor) {
        realmBridge.realm().use { realm ->
            val entity = investor.toEntity()

            realm.executeTransaction {
                it.delete(InvestorEntity::class.java)
                it.insertOrUpdate(entity)
            }

            realm.close()
        }
    }

    override suspend fun fetch(): Outcome<Investor> {
        realmBridge.realm().use { realm ->
            val entity = realm.where(InvestorEntity::class.java).findFirst()
            entity?.let {
                val investor = realm.copyFromRealm(it)
                realm.close()

                return Outcome.Success(investor.toDomain())
            }

            realm.close()
            return Outcome.Error(IOException())
        }
    }

    override fun delete() {
        realmBridge.realm().use { realm ->
            realm.executeTransaction {
                it.delete(InvestorEntity::class.java)
            }

            realm.close()
        }
    }
}