package com.kaiohenrique.empresasandroidioasys.data.extentions

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.kaiohenrique.empresasandroidioasys.BuildConfig
import com.kaiohenrique.empresasandroidioasys.data.utils.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
private const val OKHTTP_TIMEOUT = 60L

fun createOkHttpClient(): OkHttpClient {
    val clientHttp = OkHttpClient.Builder()
        .connectTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(OKHTTP_TIMEOUT, TimeUnit.SECONDS)

    if (BuildConfig.DEBUG) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        clientHttp.addInterceptor(logging)
    }

    clientHttp.addNetworkInterceptor { chain ->
        if (!chain.request().url().toString().contains(END_POINT_AUTH)) {

            val request = chain.request().newBuilder()
                .addHeader("access-token", ACCESS_TOKEN)
                .addHeader("client", CLIENT)
                .addHeader("uid", UID)
                .build()

            chain.proceed(request)
        } else {
            chain.proceed(chain.request())
        }
    }

    return clientHttp.build()
}

fun createRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
    .baseUrl(URL_BASE)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .client(okHttpClient)
    .build()

inline fun <reified T> createApiRequest(retrofit: Retrofit): T = retrofit.create(T::class.java)