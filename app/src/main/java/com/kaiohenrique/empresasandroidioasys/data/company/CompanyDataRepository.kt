package com.kaiohenrique.empresasandroidioasys.data.company

import com.kaiohenrique.empresasandroidioasys.data.extentions.apiRequest
import com.kaiohenrique.empresasandroidioasys.data.mapper.toDomain
import com.kaiohenrique.empresasandroidioasys.domain.company.CompanyRepository
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome
import java.io.IOException


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */
class CompanyDataRepository(
    private val service: CompanyService
) : CompanyRepository {

    override suspend fun fetchAll() = apiRequest { companiesRequest() }

    private suspend fun companiesRequest(): Outcome<List<Company>> {
        val res = service.fetchAll().await()
        if (res.isSuccessful) {
            res.body()?.let {
                return Outcome.Success(it.company.map { listCompanies -> listCompanies.toDomain() })
            }
        }

        return Outcome.Error(IOException(res.message()), res.code())
    }

    override suspend fun fetchByName(query: String): Outcome<List<Company>> =
        apiRequest { companiesByNameRequest(query) }

    private suspend fun companiesByNameRequest(
        query: String
    ): Outcome<List<Company>> {
        val res = service.fetchByName(query).await()
        if (res.isSuccessful) {
            res.body()?.let {
                return Outcome.Success(it.company.map { listCompanies -> listCompanies.toDomain() })
            }
        }

        return Outcome.Error(IOException(res.message()), res.code())
    }
}