package com.kaiohenrique.empresasandroidioasys.data.auth

import com.kaiohenrique.empresasandroidioasys.data.extentions.apiRequest
import com.kaiohenrique.empresasandroidioasys.data.mapper.toDomain
import com.kaiohenrique.empresasandroidioasys.domain.auth.LoginRepository
import com.kaiohenrique.empresasandroidioasys.domain.model.AuthRequest
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome
import java.io.IOException


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class LoginDataRepository(
    private val service: LoginService
) : LoginRepository {

    override suspend fun login(
        credentials: AuthRequest
    ) = apiRequest { loginRequest(credentials) }

    private suspend fun loginRequest(
        credentials: AuthRequest
    ): Outcome<Investor> {
        val res = service.login(credentials).await()
        if (res.isSuccessful) {
            res.body()?.let {
                return Outcome.Success(it.investor.toDomain())
            }
        }

        return Outcome.Error(IOException(res.message()), res.code())
    }
}