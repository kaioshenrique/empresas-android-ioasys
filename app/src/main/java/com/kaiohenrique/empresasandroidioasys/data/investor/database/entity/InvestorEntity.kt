package com.kaiohenrique.empresasandroidioasys.data.investor.database.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
open class InvestorEntity(
    @PrimaryKey var id: Int = 0,
    var investorName: String = "",
    var email: String = "",
    var city: String = "",
    var country: String = ""
) : RealmObject()