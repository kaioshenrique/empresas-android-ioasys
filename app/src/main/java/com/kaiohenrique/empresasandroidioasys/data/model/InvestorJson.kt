package com.kaiohenrique.empresasandroidioasys.data.model

import com.google.gson.annotations.SerializedName


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class InvestorJson(
    @SerializedName("id") val id: Int,
    @SerializedName("investor_name") val investorName: String,
    @SerializedName("email") val email: String,
    @SerializedName("city") val city: String,
    @SerializedName("country") val country: String
)