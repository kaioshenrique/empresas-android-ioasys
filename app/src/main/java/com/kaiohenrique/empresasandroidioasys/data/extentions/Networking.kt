package com.kaiohenrique.empresasandroidioasys.data.extentions

import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
suspend fun <T : Any> apiRequest(
    f: suspend () -> Outcome<T>
): Outcome<T> {
    return try {
        f()
    } catch (e: Exception) {
        Outcome.Error(e)
    }
}