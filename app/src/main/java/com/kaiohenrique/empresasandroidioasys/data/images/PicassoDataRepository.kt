package com.kaiohenrique.empresasandroidioasys.data.images

import com.kaiohenrique.empresasandroidioasys.domain.images.PicassoRepository
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */
class PicassoDataRepository : PicassoRepository {

    override fun fetchImageWithPicasso(path: String): RequestCreator {
        return downloadImage(path)
    }

    private fun downloadImage(path: String): RequestCreator {
        return Picasso.get()
            .load(path)
    }
}