package com.kaiohenrique.empresasandroidioasys.data.model

import com.google.gson.annotations.SerializedName


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */
data class CompanyTypeJson(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("enterprise_type_name") val companyTypeName: String = ""
)