package com.kaiohenrique.empresasandroidioasys.data.realm

import io.realm.Realm


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class RealmBridge {

    fun realm(): Realm {
        return Realm.getDefaultInstance()
    }
}