package com.kaiohenrique.empresasandroidioasys.data.investor.database

import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface InvestorDatabase {
    suspend fun save(investor: Investor)
    fun delete()
    suspend fun fetch(): Outcome<Investor>
}