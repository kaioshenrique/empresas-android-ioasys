package com.kaiohenrique.empresasandroidioasys.data.investor

import com.kaiohenrique.empresasandroidioasys.data.investor.database.InvestorDatabase
import com.kaiohenrique.empresasandroidioasys.domain.investor.InvestorRepository
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class InvestorDataRepository(
    private val database: InvestorDatabase
) : InvestorRepository {

    override suspend fun save(investor: Investor) = database.save(investor)

    override fun delete() = database.delete()

    override suspend fun fetch() = database.fetch()
}