package com.kaiohenrique.empresasandroidioasys.data.extentions

import com.kaiohenrique.empresasandroidioasys.R
import java.io.IOException


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
fun Throwable.getErroMessage(httpCode: Int? = 0): Int = when (this) {
    is IOException -> {
        when (httpCode) {
            401 -> R.string.text_invalid_session
            else -> R.string.text_unknown_error
        }
    }
    else -> R.string.text_unknown_error
}