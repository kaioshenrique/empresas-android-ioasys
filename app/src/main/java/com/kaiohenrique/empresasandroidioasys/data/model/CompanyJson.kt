package com.kaiohenrique.empresasandroidioasys.data.model

import com.google.gson.annotations.SerializedName


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class CompanyJson(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("enterprise_name") val companyName: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("photo") val pathImage: String? = "",
    @SerializedName("country") val country: String = "",
    @SerializedName("enterprise_type") val companyType: CompanyTypeJson? = null
)