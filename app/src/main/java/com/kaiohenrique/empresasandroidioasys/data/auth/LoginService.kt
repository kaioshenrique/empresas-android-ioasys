package com.kaiohenrique.empresasandroidioasys.data.auth

import com.kaiohenrique.empresasandroidioasys.data.model.InvestorDataJson
import com.kaiohenrique.empresasandroidioasys.data.utils.END_POINT_AUTH
import com.kaiohenrique.empresasandroidioasys.domain.model.AuthRequest
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface LoginService {

    @POST(END_POINT_AUTH)
    fun login(@Body credentials: AuthRequest): Deferred<Response<InvestorDataJson>>
}