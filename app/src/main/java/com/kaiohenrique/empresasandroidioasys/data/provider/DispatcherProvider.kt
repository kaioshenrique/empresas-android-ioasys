package com.kaiohenrique.empresasandroidioasys.data.provider

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

data class DispatcherProvider(
    val ui: CoroutineDispatcher,
    val io: CoroutineDispatcher
)

fun provideDispatcherProvider() = DispatcherProvider(
    Dispatchers.Main,
    Dispatchers.IO
)