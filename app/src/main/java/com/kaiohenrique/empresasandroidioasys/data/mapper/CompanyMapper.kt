package com.kaiohenrique.empresasandroidioasys.data.mapper

import com.kaiohenrique.empresasandroidioasys.data.model.CompanyJson
import com.kaiohenrique.empresasandroidioasys.data.model.CompanyTypeJson
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.domain.model.CompanyType


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */

fun Company.toJson() = CompanyJson(
    id, companyName, description, pathImage, country, companyType?.toJson()
)

fun CompanyJson.toDomain() = Company(
    id, companyName, description, pathImage, country, companyType?.toDomain()
)

fun CompanyType.toJson() = CompanyTypeJson(
    id, companyTypeName
)

fun CompanyTypeJson.toDomain() = CompanyType(
    id, companyTypeName
)