package com.kaiohenrique.empresasandroidioasys.data.mapper

import com.kaiohenrique.empresasandroidioasys.data.investor.database.entity.InvestorEntity
import com.kaiohenrique.empresasandroidioasys.data.model.InvestorJson
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

fun Investor.toEntity() = InvestorEntity(
    id, investorName, email, city, country
)

fun InvestorEntity.toDomain() = Investor(
    id, investorName, email, city, country
)

fun InvestorJson.toDomain() = Investor(
    id, investorName, email, city, country
)