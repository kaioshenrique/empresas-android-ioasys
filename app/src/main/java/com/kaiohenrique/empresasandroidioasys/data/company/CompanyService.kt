package com.kaiohenrique.empresasandroidioasys.data.company

import com.kaiohenrique.empresasandroidioasys.data.model.CompanyDataJson
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface CompanyService {

    @GET("enterprises")
    fun fetchAll(): Deferred<Response<CompanyDataJson>>

    @GET("enterprises")
    fun fetchByName(
        @Query("name") query: String
    ): Deferred<Response<CompanyDataJson>>


}