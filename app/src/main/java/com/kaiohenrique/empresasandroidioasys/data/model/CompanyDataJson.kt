package com.kaiohenrique.empresasandroidioasys.data.model

import com.google.gson.annotations.SerializedName


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class CompanyDataJson(
    @SerializedName("enterprises") val company: List<CompanyJson>
)