package com.kaiohenrique.empresasandroidioasys.domain.images

import com.squareup.picasso.RequestCreator


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */
interface PicassoRepository {
    fun fetchImageWithPicasso(path: String): RequestCreator
}