package com.kaiohenrique.empresasandroidioasys.domain.model


/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */
data class CompanyType(
    val id: Int = 0,
    val companyTypeName: String = ""
)