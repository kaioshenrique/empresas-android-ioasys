package com.kaiohenrique.empresasandroidioasys.domain.investor

import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface InvestorRepository {
    suspend fun save(investor: Investor)
    fun delete()
    suspend fun fetch(): Outcome<Investor>
}