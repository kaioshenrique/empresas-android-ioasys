package com.kaiohenrique.empresasandroidioasys.domain.model


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
sealed class Outcome<out T : Any> {

    data class Success<out T : Any>(val data: T) : Outcome<T>()
    data class Error(val exception: Exception, val httpCode: Int = -500) : Outcome<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception ($httpCode)]"
        }
    }
}