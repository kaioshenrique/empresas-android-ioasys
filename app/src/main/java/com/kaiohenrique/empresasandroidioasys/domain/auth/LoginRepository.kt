package com.kaiohenrique.empresasandroidioasys.domain.auth

import com.kaiohenrique.empresasandroidioasys.domain.model.AuthRequest
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface LoginRepository {
    suspend fun login(credentials: AuthRequest): Outcome<Investor>
}