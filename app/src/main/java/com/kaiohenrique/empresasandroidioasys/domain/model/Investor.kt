package com.kaiohenrique.empresasandroidioasys.domain.model


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class Investor(
    val id: Int = 0,
    val investorName: String = "",
    val email: String = "",
    val city: String = "",
    val country: String = ""
)