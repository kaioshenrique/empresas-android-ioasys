package com.kaiohenrique.empresasandroidioasys.domain.model

import com.squareup.picasso.RequestCreator


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class Company(
    val id: Int = 0,
    val companyName: String = "",
    val description: String = "",
    val pathImage: String? = "",
    val country: String = "",
    val companyType: CompanyType? = null
) {
    var photo: RequestCreator? = null
}