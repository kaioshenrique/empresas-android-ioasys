package com.kaiohenrique.empresasandroidioasys.domain.company

import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
interface CompanyRepository {
    suspend fun fetchAll(): Outcome<List<Company>>
    suspend fun fetchByName(query: String): Outcome<List<Company>>
}