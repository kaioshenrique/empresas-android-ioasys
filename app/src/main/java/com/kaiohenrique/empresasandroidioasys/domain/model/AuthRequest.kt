package com.kaiohenrique.empresasandroidioasys.domain.model


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
data class AuthRequest(
    val email: String,
    val password: String
)