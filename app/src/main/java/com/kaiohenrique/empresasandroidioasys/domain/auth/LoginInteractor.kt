package com.kaiohenrique.empresasandroidioasys.domain.auth

import com.kaiohenrique.empresasandroidioasys.domain.investor.InvestorRepository
import com.kaiohenrique.empresasandroidioasys.domain.model.AuthRequest
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class LoginInteractor(
    private val loginRepository: LoginRepository,
    private val investorRepository: InvestorRepository
) {

    suspend operator fun invoke(
        email: String,
        password: String
    ): Outcome<Investor> {
        val credentials = AuthRequest(email, password)

        val response = loginRepository.login(credentials)

        when (response) {
            is Outcome.Success -> investorRepository.save(response.data)
            is Outcome.Error -> response.exception
        }

        return response
    }

    fun delete() = investorRepository.delete()

    suspend fun fetchInvestor() = investorRepository.fetch()

}