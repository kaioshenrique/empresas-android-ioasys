package com.kaiohenrique.empresasandroidioasys.domain.company

import com.kaiohenrique.empresasandroidioasys.domain.images.PicassoRepository
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class CompanyInteractor(
    private val companyRepository: CompanyRepository,
    private val picassoRepository: PicassoRepository
) {

    suspend operator fun invoke(): Outcome<List<Company>> {
        val response = companyRepository.fetchAll()

        when (response) {
            is Outcome.Success -> response.data
            is Outcome.Error -> response.exception
        }

        return response
    }

    suspend fun fetchByName(query: String): Outcome<List<Company>> {
        val response = companyRepository.fetchByName(query)

        when (response) {
            is Outcome.Success -> response.data
            is Outcome.Error -> response.exception
        }

        return response
    }

    fun fetchImage(path: String) =
        picassoRepository.fetchImageWithPicasso(path)

}