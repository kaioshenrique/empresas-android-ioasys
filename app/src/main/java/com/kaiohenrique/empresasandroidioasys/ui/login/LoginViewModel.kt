package com.kaiohenrique.empresasandroidioasys.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kaiohenrique.empresasandroidioasys.data.extentions.getErroMessage
import com.kaiohenrique.empresasandroidioasys.data.provider.DispatcherProvider
import com.kaiohenrique.empresasandroidioasys.domain.auth.LoginInteractor
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class LoginViewModel(
    private val loginInteractor: LoginInteractor,
    private val dispatcherProvider: DispatcherProvider
) : ViewModel() {

    private var mainJob: Job = Job()
    private var loginJob: Job? = null
    private var investorJob: Job? = null

    private val scope: CoroutineScope = CoroutineScope(dispatcherProvider.ui + mainJob)

    private val investor = MutableLiveData<Investor>()
    fun investor(): LiveData<Investor> = investor

    private val errorMessage = MutableLiveData<Int>()
    fun errorMessage(): LiveData<Int> = errorMessage

    private val loading = MutableLiveData<Boolean>()
    fun loading(): LiveData<Boolean> = loading

    fun verifyInvestorExists() {
        if (investorJob?.isActive == true) return
        investorJob = launchInvestor()
    }

    private fun launchInvestor() = scope.launch(dispatcherProvider.io) {
        val response = loginInteractor.fetchInvestor()

        withContext(dispatcherProvider.ui) {
            when (response) {
                is Outcome.Success -> investor.value = response.data
                is Outcome.Error -> investor.value = null
            }
        }

    }

    fun auth(
        email: String,
        password: String
    ) {
        if (loginJob?.isActive == true) return
        loginJob = launchAuth(email, password)
    }

    private fun launchAuth(
        email: String,
        password: String
    ) = scope.launch(dispatcherProvider.io) {
        withContext(dispatcherProvider.ui) { toggleLoading(true) }

        val response = loginInteractor(email, password)

        withContext(dispatcherProvider.ui) {
            toggleLoading(false)
            when (response) {
                is Outcome.Success -> investor.value = response.data
                is Outcome.Error -> errorMessage.value = response.exception.getErroMessage(response.httpCode)
            }
        }
    }

    private fun toggleLoading(flag: Boolean) {
        loading.value = flag
    }

    override fun onCleared() {
        super.onCleared()
        mainJob.cancel()
        loginJob?.cancel()
        investorJob?.cancel()
    }

}