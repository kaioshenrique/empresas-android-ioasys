package com.kaiohenrique.empresasandroidioasys.ui.extentions

import android.view.View

/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */

fun View.toVisible() {
    this.visibility = View.VISIBLE
}

fun View.toGone() {
    this.visibility = View.GONE
}

fun View.toInvisible() {
    this.visibility = View.INVISIBLE
}