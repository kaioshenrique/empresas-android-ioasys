package com.kaiohenrique.empresasandroidioasys.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kaiohenrique.empresasandroidioasys.R
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import kotlinx.android.synthetic.main.item_companies_list.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class CompanyAdapter(
    private val callback: (Company) -> Unit
) : RecyclerView.Adapter<CompaniesViewHolder>() {

    private var companies: List<Company> = emptyList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CompaniesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_companies_list, parent, false)
        return CompaniesViewHolder(view)
    }

    override fun getItemCount() = companies.size

    override fun onBindViewHolder(holder: CompaniesViewHolder, position: Int) {
        holder.bind(companies[position], callback)
    }

    fun setList(list: List<Company>) {
        companies = list
        notifyDataSetChanged()
    }
}

class CompaniesViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView), CoroutineScope {

    override val coroutineContext: CoroutineContext = Dispatchers.Main

    fun bind(company: Company, callback: (Company) -> Unit) {
        with(itemView) {
            textCompanyName.text = company.companyName
            textCompanyRole.text = company.companyType?.companyTypeName
            textCompanyCountry.text = company.country
            company.photo?.into(imageCompany)
        }

        itemView.setOnClickListener { callback.invoke(company) }
    }

}