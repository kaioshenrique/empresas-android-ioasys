package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.ui.company.CompanyViewModel
import com.kaiohenrique.empresasandroidioasys.ui.login.LoginViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

val uiModule = module {
    viewModel { LoginViewModel(get(), get()) }
    viewModel { CompanyViewModel(get(), get(), get()) }
}