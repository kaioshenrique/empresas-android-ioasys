package com.kaiohenrique.empresasandroidioasys.ui.company

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kaiohenrique.empresasandroidioasys.data.extentions.getErroMessage
import com.kaiohenrique.empresasandroidioasys.data.provider.DispatcherProvider
import com.kaiohenrique.empresasandroidioasys.domain.auth.LoginInteractor
import com.kaiohenrique.empresasandroidioasys.domain.company.CompanyInteractor
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.domain.model.Outcome
import com.squareup.picasso.RequestCreator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class CompanyViewModel(
    private val companyInteractor: CompanyInteractor,
    private val loginInteractor: LoginInteractor,
    private val dispatcherProvider: DispatcherProvider
) : ViewModel() {

    private var mainJob: Job = Job()
    private var companyJob: Job? = null

    private val scope: CoroutineScope = CoroutineScope(dispatcherProvider.ui + mainJob)

    private val companies = MutableLiveData<List<Company>>()
    fun companies(): LiveData<List<Company>> = companies

    private val errorMessage = MutableLiveData<Int>()
    fun errorMessage(): LiveData<Int> = errorMessage

    fun deleteInvestor() = loginInteractor.delete()

    fun fetchCompanies() {
        if (companyJob?.isActive == true) return
        companyJob = launchFetchCompanies()
    }

    fun fetchByName(query: String) {
        if (companyJob?.isActive == true) return
        companyJob = launchFetchByName(query)
    }

    private fun launchFetchByName(query: String) = scope.launch(dispatcherProvider.io) {
        val response = companyInteractor.fetchByName(query)

        withContext(dispatcherProvider.ui) {
            when (response) {
                is Outcome.Success -> companies.value = response.data
                is Outcome.Error -> companies.value = emptyList()
            }
        }
    }

    private fun launchFetchCompanies() = scope.launch(dispatcherProvider.io) {
        val response = companyInteractor()

        withContext(dispatcherProvider.ui) {
            when (response) {
                is Outcome.Success -> companies.value = response.data
                is Outcome.Error -> errorMessage.value = response.exception.getErroMessage(response.httpCode)
            }
        }

    }

    fun getImage(path: String): RequestCreator {
        return companyInteractor.fetchImage(path)
    }

    override fun onCleared() {
        super.onCleared()
        mainJob.cancel()
        companyJob?.cancel()
    }

}