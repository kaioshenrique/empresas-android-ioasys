package com.kaiohenrique.empresasandroidioasys.ui.company

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kaiohenrique.empresasandroidioasys.R
import com.kaiohenrique.empresasandroidioasys.data.utils.URL_BASE_IMAGE
import com.kaiohenrique.empresasandroidioasys.domain.model.Company
import com.kaiohenrique.empresasandroidioasys.ui.adapter.CompanyAdapter
import com.kaiohenrique.empresasandroidioasys.ui.dialog.CustomDialogFragment
import com.kaiohenrique.empresasandroidioasys.ui.dialog.LogoutDialogFragment
import com.kaiohenrique.empresasandroidioasys.ui.extentions.intentFor
import com.kaiohenrique.empresasandroidioasys.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_company.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class CompanyActivity : AppCompatActivity() {

    private val viewModel: CompanyViewModel by viewModel()

    private val adapter by lazy { CompanyAdapter(::callbackClickItem) }

    private val logoutFragment by lazy {
        LogoutDialogFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_company)
        setupToolbar()

        with(viewModel) {
            companies().observe(this@CompanyActivity, Observer(::observerCompanies))
            errorMessage().observe(this@CompanyActivity, Observer(::observerError))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        logoutDialog()
    }

    private fun observerError(errorMessage: Int?) {
        errorMessage?.let {
            showDialogMessage(errorMessage)
        }
    }

    private fun showDialogMessage(message: Int) {
        val dialog = CustomDialogFragment.newInstance(
            getString(R.string.text_attention),
            getString(message)
        )

        showDialogAlert(dialog)
    }

    private fun showDialogAlert(dialog: CustomDialogFragment) {
        if (dialog.isVisible) return
        dialog.show(supportFragmentManager, dialog.tag)
        dialog.onDismiss { viewModel.fetchCompanies() }
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchCompanies()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun observerCompanies(companies: List<Company>?) {
        companies?.let {
            setupRecyclerView(it)
        }
    }

    private fun setupRecyclerView(companies: List<Company>) {
        adapter.setList(companies)
        companiesList.layoutManager = LinearLayoutManager(this)
        companies.map {
            it.photo = viewModel.getImage(URL_BASE_IMAGE + it.pathImage)
        }
        companiesList.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.item_search, menu)

        val itemSearch = menu.findItem(R.id.action_search)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager

        var searchView: SearchView? = null
        if (searchView != null) {
            searchView = itemSearch.actionView as SearchView
            observerTextSearch(searchView)
        }
        searchView?.setSearchableInfo(searchManager.getSearchableInfo(componentName))

        return super.onCreateOptionsMenu(menu)
    }

    private fun observerTextSearch(textSearch: SearchView) {
        textSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.fetchByName(query ?: "")
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

    }

    private fun callbackClickItem(company: Company) {
        intentFor<DetailCompanyActivity>()
            .putExtra(DetailCompanyActivity.EXTRA_NAME, company.companyName)
            .putExtra(DetailCompanyActivity.EXTRA_IMAGE, company.pathImage)
            .putExtra(DetailCompanyActivity.EXTRA_DESCRIPTION, company.description)
            .run { startActivity(this) }
    }

    private fun logoutDialog() {
        if (!logoutFragment.isVisible) {
            logoutFragment.onLogoutCallback {
                intentFor<LoginActivity>()
                    .run {
                        viewModel.deleteInvestor()
                        startActivity(this)
                        finishAffinity()
                    }
            }
            logoutFragment.show(supportFragmentManager, logoutFragment.tag)
        }
    }
}