package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.data.auth.LoginDataRepository
import com.kaiohenrique.empresasandroidioasys.data.company.CompanyDataRepository
import com.kaiohenrique.empresasandroidioasys.data.images.PicassoDataRepository
import com.kaiohenrique.empresasandroidioasys.data.investor.InvestorDataRepository
import com.kaiohenrique.empresasandroidioasys.domain.auth.LoginRepository
import com.kaiohenrique.empresasandroidioasys.domain.company.CompanyRepository
import com.kaiohenrique.empresasandroidioasys.domain.images.PicassoRepository
import com.kaiohenrique.empresasandroidioasys.domain.investor.InvestorRepository
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

val repositoryModule = module {
    single<LoginRepository> { LoginDataRepository(get()) }
    single<InvestorRepository> { InvestorDataRepository(get()) }
    single<CompanyRepository> { CompanyDataRepository(get()) }
    single<PicassoRepository> { PicassoDataRepository() }
}