package com.kaiohenrique.empresasandroidioasys.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.kaiohenrique.empresasandroidioasys.R
import kotlinx.android.synthetic.main.fragment_dialog_logout.*

/**
 *
 *@author Kaio Henrique on 10/07/2019
 *
 */

class LogoutDialogFragment : DialogFragment() {

    private var onLogout: (() -> Unit)? = null

    override fun getTheme(): Int {
        return R.style.AppTheme_Dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.fragment_dialog_logout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        actionCancel.setOnClickListener {
            dismiss()
        }

        actionLogout.setOnClickListener {
            logout()
        }
    }

    companion object {
        fun newInstance(): LogoutDialogFragment {
            val fragment = LogoutDialogFragment()
            val bundle = Bundle()
            fragment.arguments = bundle
            return fragment
        }
    }

    fun logout() {
        onLogout?.invoke()
        dismiss()
    }

    fun onLogoutCallback(function: () -> Unit) {
        onLogout = function
    }

    fun hide() {
        if (dialog.isShowing) {
            dismiss()
        }
    }

}