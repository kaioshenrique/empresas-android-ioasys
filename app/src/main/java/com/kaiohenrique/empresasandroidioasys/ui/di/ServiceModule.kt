package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.data.auth.LoginService
import com.kaiohenrique.empresasandroidioasys.data.company.CompanyService
import com.kaiohenrique.empresasandroidioasys.data.extentions.createApiRequest
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

val serviceModule = module {
    single { createApiRequest<LoginService>(get()) }
    single { createApiRequest<CompanyService>(get()) }
}