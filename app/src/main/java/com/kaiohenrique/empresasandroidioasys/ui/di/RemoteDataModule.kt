package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.data.extentions.createOkHttpClient
import com.kaiohenrique.empresasandroidioasys.data.extentions.createRetrofit
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

val remoteModule = module {
    single { createOkHttpClient() }
    single { createRetrofit(get()) }
}