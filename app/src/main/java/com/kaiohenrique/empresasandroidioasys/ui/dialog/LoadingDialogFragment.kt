package com.kaiohenrique.empresasandroidioasys.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.kaiohenrique.empresasandroidioasys.R
import com.kaiohenrique.empresasandroidioasys.ui.extentions.changeColor
import kotlinx.android.synthetic.main.fragment_loading.*


/**
 *
 *@author Kaio Henrique on 08/07/2019
 *
 */
class LoadingDialogFragment : DialogFragment() {

    override fun getTheme(): Int {
        return R.style.AppTheme_Dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.fragment_loading, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progress.changeColor(ContextCompat.getColor(requireContext(), R.color.mediumPink))
    }

}