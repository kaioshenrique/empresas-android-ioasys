package com.kaiohenrique.empresasandroidioasys.ui.company

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.kaiohenrique.empresasandroidioasys.R
import com.kaiohenrique.empresasandroidioasys.data.utils.URL_BASE_IMAGE
import kotlinx.android.synthetic.main.activity_detail_company.*
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class DetailCompanyActivity : AppCompatActivity() {

    private val viewModel: CompanyViewModel by viewModel()

    companion object {
        const val EXTRA_NAME = "EXTRA_NAME"
        const val EXTRA_IMAGE = "EXTRA_IMAGE"
        const val EXTRA_DESCRIPTION = "EXTRA_DESCRIPTION"
    }

    private val imageDefault by lazy {
        ContextCompat.getDrawable(this, R.drawable.ic_cant_download_image)
    }

    private val name by lazy {
        intent.getStringExtra(EXTRA_NAME)
    }

    private val imageUrl by lazy {
        intent.getStringExtra(EXTRA_IMAGE)
    }

    private val description by lazy {
        intent.getStringExtra(EXTRA_DESCRIPTION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_company)
        setupToolbar()
        configureView()
    }

    private fun setupToolbar() {
        with(toolbar) {
            setSupportActionBar(this)
            supportActionBar?.setDisplayShowTitleEnabled(false)
            setNavigationOnClickListener { onBackPressed() }
        }
    }

    private fun configureView() {
        companyName.text = name
        descriptionCompany.text = description
        descriptionCompany.movementMethod = ScrollingMovementMethod()
        setImageContent()
    }

    private fun setImageContent() {
        viewModel.getImage(URL_BASE_IMAGE + imageUrl).into(imageDetailCompany)
    }
}