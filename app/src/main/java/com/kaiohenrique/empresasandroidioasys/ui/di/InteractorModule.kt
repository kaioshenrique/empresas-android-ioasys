package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.domain.auth.LoginInteractor
import com.kaiohenrique.empresasandroidioasys.domain.company.CompanyInteractor
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */

val interactorModule = module {
    single { LoginInteractor(get(), get()) }
    single { CompanyInteractor(get(), get()) }
}