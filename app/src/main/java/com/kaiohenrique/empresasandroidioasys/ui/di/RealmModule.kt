package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.data.investor.database.InvestorDatabase
import com.kaiohenrique.empresasandroidioasys.data.investor.database.InvestorRealmDatabase
import com.kaiohenrique.empresasandroidioasys.data.realm.RealmBridge
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
val realmModule = module {
    single { RealmBridge() }
    single<InvestorDatabase> { InvestorRealmDatabase(get()) }
}