package com.kaiohenrique.empresasandroidioasys.ui.application

import android.app.Application
import com.kaiohenrique.empresasandroidioasys.ui.di.*
import io.realm.Realm
import org.koin.android.ext.android.startKoin


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
class App : Application() {

    private val appModules by lazy {
        listOf(
            remoteModule,
            serviceModule,
            realmModule,
            repositoryModule,
            interactorModule,
            dispatcherProviderModule,
            uiModule
        )
    }

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        startKoin(this, appModules)
    }
}