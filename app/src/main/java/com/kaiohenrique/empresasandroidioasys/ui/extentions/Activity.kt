package com.kaiohenrique.empresasandroidioasys.ui.extentions

import android.app.Activity
import android.content.Intent


/**
 *
 *@author Kaio Henrique on 07/07/2019
 *
 */

inline fun <reified T : Activity> Activity.intentFor(): Intent {
    return Intent(this, T::class.java)
}