package com.kaiohenrique.empresasandroidioasys.ui.extentions

import android.widget.ProgressBar
import androidx.annotation.ColorInt


fun ProgressBar.changeColor(@ColorInt cor: Int) {
    val progressDrawable = indeterminateDrawable.mutate()
    progressDrawable.setColorFilter(cor, android.graphics.PorterDuff.Mode.SRC_IN)
    setProgressDrawable(progressDrawable)
}