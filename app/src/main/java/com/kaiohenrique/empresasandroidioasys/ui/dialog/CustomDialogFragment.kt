package com.kaiohenrique.empresasandroidioasys.ui.dialog

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.kaiohenrique.empresasandroidioasys.R
import kotlinx.android.synthetic.main.fragment_custom_dialog.*


/**
 *
 *@author Kaio Henrique on 08/07/2019
 *
 */
class CustomDialogFragment : DialogFragment() {

    private val TITLE = "titulo"
    private val MESSAGE = "mensagem"
    private val ACTION = "confirmar"

    private val title by lazy {
        arguments?.getString(TITLE)
    }

    private val message by lazy {
        arguments?.getString(MESSAGE)
    }

    private val action by lazy {
        arguments?.getString(ACTION)
    }

    private var onDismiss: (() -> Unit)? = null

    private var onDialogDismiss: (() -> Unit)? = null

    override fun getTheme(): Int {
        return R.style.AppTheme_Dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.fragment_custom_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        writeTexts()
        actionConfirmDialog.setOnClickListener(::onClickButton)
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        onDialogDismiss?.invoke()
    }

    companion object {
        fun newInstance(
            title: String,
            message: String,
            action: String = "Ok",
            titleCaps: Boolean = false
        ): CustomDialogFragment {
            val fragment = CustomDialogFragment()
            val bundle = Bundle()
            bundle.putString(fragment.TITLE, if (titleCaps) title.toUpperCase() else title)
            bundle.putString(fragment.MESSAGE, message)
            bundle.putString(fragment.ACTION, action)
            fragment.arguments = bundle
            return fragment
        }
    }

    private fun onClickButton(view: View) {
        when (view.id) {
            actionConfirmDialog.id -> {
                onDismiss?.invoke()
                dismiss()
            }
        }
    }

    private fun writeTexts() {
        titleDialog.text = title
        messageDialog.text = message
        actionConfirmDialog.text = action
    }

    fun onDismiss(f: () -> Unit) {
        onDismiss = f
    }

    fun onDialogDismiss(f: () -> Unit) {
        onDialogDismiss = f
    }
}