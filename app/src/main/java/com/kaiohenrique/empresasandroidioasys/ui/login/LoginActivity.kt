package com.kaiohenrique.empresasandroidioasys.ui.login

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.kaiohenrique.empresasandroidioasys.R
import com.kaiohenrique.empresasandroidioasys.domain.model.Investor
import com.kaiohenrique.empresasandroidioasys.ui.company.CompanyActivity
import com.kaiohenrique.empresasandroidioasys.ui.dialog.CustomDialogFragment
import com.kaiohenrique.empresasandroidioasys.ui.dialog.LoadingDialogFragment
import com.kaiohenrique.empresasandroidioasys.ui.extentions.intentFor
import convalida.ktx.isEmail
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()

    private val emailValidator by lazy {
        inputEmail.isEmail(errorMessage = getString(R.string.text_error_email_validator))
    }

    private val loadingFragment by lazy {
        LoadingDialogFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        actionLogin.setOnClickListener { login() }

        with(viewModel) {
            emailValidator
            verifyInvestorExists()
            investor().observe(this@LoginActivity, Observer(::onInvestor))
            loading().observe(this@LoginActivity, Observer(::onLoading))
            errorMessage().observe(this@LoginActivity, Observer(::onErrorMessage))
        }
    }

    private fun onInvestor(investor: Investor?) {
        investor?.let {
            intentFor<CompanyActivity>()
                .run { startActivity(this) }
        }
    }

    private fun onLoading(flag: Boolean?) {
        val isLoading = flag ?: false
        if (isLoading) showLoading() else hideLoading()
    }

    private fun onErrorMessage(errorMessage: Int?) {
        errorMessage?.let {
            showDialogMessage(errorMessage)
        }
    }

    private fun showLoading() {
        if (loadingFragment.isVisible) return

        loadingFragment.isCancelable = false
        loadingFragment.show(supportFragmentManager, loadingFragment.tag)
    }

    private fun hideLoading() {
        if (loadingFragment.isVisible) {
            loadingFragment.dismiss()
        }
    }

    private fun login() {
        if (validateFields()) {
            viewModel.auth(inputEmail.text.toString(), inputPassword.text.toString())
        } else {
            showDialogMessage(R.string.text_fields_required)
        }
    }

    private fun showDialogMessage(message: Int) {
        val dialog = CustomDialogFragment.newInstance(
            getString(R.string.text_attention),
            getString(message)
        )

        showDialogAlert(dialog)
    }

    private fun showDialogAlert(dialog: CustomDialogFragment) {
        if (dialog.isVisible) return
        dialog.show(supportFragmentManager, dialog.tag)
    }

    private fun validateFields() =
        (emailValidator.isValid(inputEmail.text.toString()) && inputPassword.text.toString().isNotEmpty())
}
