package com.kaiohenrique.empresasandroidioasys.ui.di

import com.kaiohenrique.empresasandroidioasys.data.provider.provideDispatcherProvider
import org.koin.dsl.module.module


/**
 *
 *@author Kaio Henrique on 09/07/2019
 *
 */
val dispatcherProviderModule = module {
    single { provideDispatcherProvider() }
}