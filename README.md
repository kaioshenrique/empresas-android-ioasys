**KAIO H. - TESTE PARA DESENVOLVEDOR ANDROID PARA IOASYS**

## Dependências

Principais libs / dependências utilizadas

1. **Androidx** > Jetpack de dependências disponibilizado pela própria Google.
2. **Koin** > Injeção de Dependência (DI).
3. **Coroutines** > Ultilizado como boa prática para tarefas suspensas e não bloqueantes.
4. **Retrofit + GSON + Logging-Interceptor** > Consumir API + Serializar / Desserializar objetos Json + Imprimir log de requisições
5. **Picasso** > Lib usada para realizar downloads de imagens fornecidas pela API
6. **Convalida** > Lib de validação de campos (No teste utilizei no campo de e-mail, no Login)
7. **Google Material** > Para componentes como CardView, Buttons etc.

## Arquitetura e Padrão de Projeto

1. **MVVM**
2. **Repository**

## Como Rodar

1. Pode fazer um fork e logo após fazer um `git clone` ou simplesmente executar direto um `git clone` do projeto, colocando em um workspace de sua preferência.
2. Depois abrir o projeto com o Android Studio `File` > `Open` e selecionar a pasta onde fez o `git clone`.
3. Feito isso, só rodar a aplicação no `Run`.

## Se eu tivesse mais tempo...

1. Concluiria o desafio com o restante das funcionalidades (Filtro de pesquisa).
2. Daria mais atenção ao layout.
3. Focaria nos testes unitários, mockando os repositórios para testar a lógica dos mesmos.
